#include <stdio.h>
#include <stdint.h>

int main(void)
{
	int16_t	tmp, x;
	int16_t	dir=tmp-((tmp/128)*256);

	float setPoint = 3;
	int32_t fullCircle = 360;
	int32_t halfCircle = fullCircle / 2;

	for(x=0;x<fullCircle;x++){
        tmp = x - setPoint;
        dir = tmp - ( floor(tmp/halfCircle) * fullCircle );
        printf("distance from %d to %.0f is %d\n", x, setPoint, dir);
    }

    return 0;

}
