/**
 *	To get distance and direction from a value to a fixed set point.
 * 	negative number means the value is on the 'left' side of set point
 * 	positive number means the value is on the 'right' side of set point
 *	@params {number} from - a number which we want to find the distance
 *	@params {number} to - fixed set point
 *	@params {number} fullCircle - total step needed to make full circle, must be positive number
 * */
function findDistance(from, to, fullCircle = 360){
	const halfCircle = fullCircle / 2;
	const tmp = from - to;
	const abs = Math.abs(tmp);

	const negativeSign = tmp / abs;
	const distance = negativeSign * (abs - (Math.floor(abs / halfCircle) * fullCircle));

	return isNaN(distance) ? 0 :distance;
}

(async() => {
	const fullCircle = 64;
	const to = 5;

	for(let from = 0; from < fullCircle; from++){
		console.log('distance from', from, 'to', to, 'is', findDistance(from, to, fullCircle));
	}
})();
